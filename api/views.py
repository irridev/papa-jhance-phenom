from flask_restful import Resource, reqparse

import random 

parser = reqparse.RequestParser()
parser.add_argument('file_name')


# {
#     "Test" : 1,
#     "grain_weight": 1.0,
#     "grain_length": 1.0,
#     "lwd": 1.0,
#     "seedling": 1,
#     "confidence": (0.01 * 100)
# }


class TrainedModel(Resource):
    def get(self):
        args = parser.parse_args()

        file_name = args['file_name']

        print(file_name)

        a = pd.read_csv( ('./upload/' + file_name) )
        print(a[1:1])
        return {"a": a[1:1]}

    # def get(self):

    #     args = parser.parse_args()

    #     file_name = args['file_name']
    #     test_num = args['test_num']

    #     results = []
    #     predict = []

    #     print(args)
    #     for i in range(0, test_num):
    #         results.append(i)
    #         predict.append(random.random())

    #     return {
    #         "results" : {
    #             "name": file_name,
    #             "test_num":test_num,
    #             "results": results,
    #             "predictions": predict
    #         }
    #     }

import pandas as pd

class Test(Resource):
    def get(self):
        args = parser.parse_args()

        file_name = args['file_name']
        a = pd.read_csv( ('../web/upload/' + file_name) )
        print(a[1:1])
        return a[1:1]