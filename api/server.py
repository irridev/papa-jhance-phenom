from flask import Flask
from flask_restful import Api

app = Flask(__name__)
api = Api(app)

from views import *

from flask_restful import Resource
class Hi(Resource):
    def get(self):
        return 'hi'

api.add_resource(TrainedModel, '/api/v1/trained_model')
api.add_resource(Test, '/a')
api.add_resource(Hi, '/')

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)