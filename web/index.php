
<?php

// file name

// echo "<pre>";
// echo var_dump($_POST);
// echo var_dump($_FILES);
// echo "</pre>";




function predict($file_name) {
  $curl = curl_init();


  $result = curl_exec($curl);

  curl_setopt($curl, CURLOPT_URL, 'localhost:5000/api/v1/trained_model?file_name=' . $file_name);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

  $result = curl_exec($curl);

  curl_close($curl);

  echo $result;
}


if (isset($_POST['upload_button'])) {
  $filename = $_FILES['file']['name'];

  // Location
  $location = '../api/upload/'.$filename;
  
  // file extension
  $file_extension = pathinfo($location, PATHINFO_EXTENSION);
  $file_extension = strtolower($file_extension);
  
  // Valid image extensions
  $csv_ext = array("csv");
  
  $response = "Upload Failed!";

  if(in_array($file_extension, $csv_ext)){
    // Upload file
    if(move_uploaded_file($_FILES['file']['tmp_name'],$location)){
      $response = $filename . " Successfuly uploaded!";
      predict($filename); //HERE PREDIIIIICT!!!!
    }
  }

  
  echo $response;
}


?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Pheno</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">


  <!-- Custom fonts -->
  <link href="//fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700|Ropa+Sans&display=swap" rel="stylesheet">
</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <img src="app_logo_light.png" alt="Logo" style="width:30px;height:30px;">
    <a class="navbar-brand mr-1" href="index.html" style="padding-left:10px">Pheno</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
      <div class="input-group">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadModal">Upload CSV</button>
      </div>
    </form>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
    </ul>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>
        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Test Results</div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Test</th>
                    <th>Grain Weight</th>
                    <th>Grain Width</th>
                    <th>Grain Length</th>
                    <th>LWD</th>
                    <th>Seedling</th>
                    <th>Confidence</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Test</th>
                    <th>Grain Weight</th>
                    <th>Grain Width</th>
                    <th>Grain Length</th>
                    <th>LWD</th>
                    <th>Seedling</th>
                    <th>Confidence</th>
                  </tr>
                </tfoot>
                <tbody>
                <?php
                  //for loop here
                ?>
                  <!--<tr>
                    <td>1</td>
                    <td>2.6</td>
                    <td>3.1</td>
                    <td>8.2</td>
                    <td>1.4</td>
                    <td>30</td>
                    <td>40%</td>
                  </tr>-->


                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Internation Rice Research Institute 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <!-- Upload Modal -->
  <div id="uploadModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">File upload form</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <!-- Form -->
          <form method='post' action='' enctype="multipart/form-data">
            Select file : <input type='file' name='file' id='file' class='form-control'><br>
            <input name='upload_button' type='submit' class='btn btn-primary' style="text-align: center" value='Upload' id='upload'>Upload</button>
          </form>

          <!-- Preview-->
          <div id='preview'></div>
        </div>

      </div>

    </div>
  </div>


  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Page level plugin JavaScript-->
  <script src="vendor/chart.js/Chart.min.js"></script>
  <script src="vendor/datatables/jquery.dataTables.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin.min.js"></script>

  <!-- Demo scripts for this page-->
  <script src="js/demo/datatables-demo.js"></script>
  <script src="js/demo/chart-area-demo.js"></script>

</body>

</html>